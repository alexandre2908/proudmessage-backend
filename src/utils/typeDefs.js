import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type Query {
    Users: [User!]!
    User(_id: String): User!
    Demandes: [Demande!]!
    Demande(_id: String!): Demande!
    UserDemandes(_id: String!): [Demande!]!
    MessageByUsers(from: String!, to: String!): [Message!]!
    GetLastMessage(from: String!, to: String!): Message!
  }

  type AuthData {
    name: String!
    userId: ID!
    token: String!
  }

  type Message {
    _id: ID!
    from: User!
    to: User!
    message: String!
    createdAt: String
  }

  type Demande {
    _id: ID!
    from: User!
    to: User!
    etat: String!
    createdAt: String
    updatedAt: String
  }

  type User {
    _id: ID!
    name: String!
    email: String!
    telephone: String!
    password: String!
    demandes: [Demande!]!
    contacts: [User!]!
    is_online: Boolean
    createdAt: String
    updatedAt: String
  }

  type Mutation {
    createUser(
      name: String!
      email: String!
      telephone: String!
      password: String!
    ): User!
    Login(email: String!, password: String!): AuthData
    createDemande(from: String!, to: String!): Demande!
    deleteDemande(_id: String): Demande!
    updateDemande(_id: String!, etat: String!): Demande!
    addMessage(from: String!, to: String!, message: String!): Message!
  }
`;
