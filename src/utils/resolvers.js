import { UserModel } from "../Models/UserModel";
import { DemandeModel } from "../Models/DemandeModel";
import { MessageModel } from "../Models/MessageModel";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const userFind = async (userId) => {
  const user = await UserModel.findById(userId);
  return { ...user._doc };
};
const usersFind = async (userIds) => {
  const users = await UserModel.find({ _id: { $in: userIds } });
  return users.map((user) => {
    return { ...user._doc };
  });
};

const demandesFind = async (demandesIds) => {
  const demandes = await DemandeModel.find({ _id: { $in: demandesIds } });
  return demandes.map((demande) => {
    return {
      ...demande._doc,
      from: userFind.bind(this, demande.from),
      to: userFind.bind(this, demande.to),
    };
  });
};

export const resolvers = {
  Query: {
    Users: async () => {
      const allUsers = await UserModel.find();
      return allUsers.map((users) => {
        return {
          ...users._doc,
          password: "",
          demandes: demandesFind.bind(this, users.demandes),
          contacts: usersFind.bind(this, users.contacts),
        };
      });
    },
    User: async (_, { _id }) => {
      const user = await UserModel.findById(_id);
      return {
        ...user._doc,
        demandes: demandesFind.bind(this, user.demandes),
        contacts: usersFind.bind(this, user.contacts),
      };
    },
    Demandes: async () => {
      const listDemandes = await DemandeModel.find();
      return listDemandes.map((demande) => {
        return {
          ...demande._doc,
          from: userFind.bind(this, demande.from),
          to: userFind.bind(this, demande.to),
        };
      });
    },
    UserDemandes: async (_, { _id }) => {
      const currentUser = await UserModel.findById(_id);
      if (!currentUser) throw new Error("Utilisateur n'existe pas");
      const alldemandes = await demandesFind(currentUser._doc.demandes);
      return alldemandes;
    },
    MessageByUsers: async (_, { from, to }) => {
      const fromUser = await UserModel.findById(from);
      const toUser = await UserModel.findById(to);

      if (!fromUser || !toUser) throw new Error("L'utilisateur n'existe pas");

      const messagebyUsers = await MessageModel.find({
        $or: [
          { from: { _id: from }, to: { _id: to } },
          { from: { _id: to }, to: { _id: from } },
        ],
      });
      return messagebyUsers.map((message) => {
        return {
          ...message._doc,
          from: userFind.bind(this, message.from._id),
          to: userFind.bind(this, message.to._id),
        };
      });
    },
    GetLastMessage: async (_, { from, to }) => {
      const fromUser = await UserModel.findById(from);
      const toUser = await UserModel.findById(to);

      if (!fromUser || !toUser) throw new Error("L'utilisateur n'existe pas");
      let messagebyUsers = await MessageModel.find({
        $or: [
          { from: { _id: from }, to: { _id: to } },
          { from: { _id: to }, to: { _id: from } },
        ],
      })
        .sort({ createdAt: -1 })
        .limit(1);

      if (!messagebyUsers.length) throw new Error("Aucun Message");
      messagebyUsers = messagebyUsers[0];
      return {
        ...messagebyUsers._doc,
        from: userFind.bind(this, messagebyUsers.from._id),
        to: userFind.bind(this, messagebyUsers.to._id),
      };
    },
  },
  Mutation: {
    createUser: async (_, { name, password, telephone, email }, context) => {
      const isExist = await UserModel.findOne({ email: email });
      if (isExist) throw new Error("The user already exists");

      const salt = await bcrypt.genSalt(12);
      const hashedpassword = await bcrypt.hash(password, salt);
      const newUser = new UserModel({
        name: name,
        password: hashedpassword,
        telephone: telephone,
        email: email,
      });

      const savedUser = await newUser.save();
      return { ...savedUser._doc, password: "" };
    },
    createDemande: async (_, { from, to }) => {
      const fromUser = await UserModel.findById(from);
      const toUser = await UserModel.findById(to);

      if (!fromUser) throw new Error("This user does not exist");
      if (!toUser) throw new Error("the user of the demande doesn't exists");

      const isExistTo = await DemandeModel.find({
        from: fromUser,
        to: toUser,
      });

      const isExistfrom = await DemandeModel.find({
        from: toUser,
        to: fromUser,
      });

      if (isExistTo.length || isExistfrom.length)
        throw new Error("cette demande existe deja");

      const newDemande = new DemandeModel({
        from: fromUser,
        to: toUser,
        etat: "En cours",
      });

      fromUser._doc.demandes.push(newDemande);
      const updatedUser = await fromUser.save();

      toUser._doc.demandes.push(newDemande);
      const updatedToUser = await toUser.save();

      const savedDemande = await newDemande.save();
      return savedDemande;
    },
    Login: async (_, { email, password }) => {
      const user = await UserModel.findOne({ email });
      if (!user) throw new Error("user does not exists");

      const isEqual = await bcrypt.compare(password, user.password);
      if (!isEqual) throw new Error("user does not exists");

      const token = jwt.sign(
        { user_id: user._id, email: user.email },
        process.env.TOKEN_SECRET
      );

      return { name: user.name, userId: user._id, token: token };
    },
    updateDemande: async (_, { _id, etat }) => {
      const currentDemande = await DemandeModel.findById(_id);
      if (!currentDemande) throw new Error("the demandes doesn't exists");

      if (etat === "accepted") {
        const fromuser = await UserModel.findById(currentDemande.from._id);
        const toUser = await UserModel.findById(currentDemande.to._id);

        fromuser.contacts.push(toUser);
        toUser.contacts.push(fromuser);

        const updatedFrom = await fromuser.save();
        const updatedTo = await toUser.save();
      }

      currentDemande.etat = etat;
      const savedDemande = await currentDemande.save();
      return {
        ...savedDemande,
        etat,
        from: userFind.bind(this, savedDemande.from),
        to: userFind.bind(this, savedDemande.to),
      };
    },

    addMessage: async (_, { from, to, message }) => {
      const fromUser = await UserModel.findById(from);
      const toUser = await UserModel.findById(to);

      if (!fromUser || !toUser)
        throw new Error("Ces utilisateurs n'existe pas");

      const newMessage = new MessageModel({
        from: fromUser,
        to: toUser,
        message: message,
      });
      const savedMessage = await newMessage.save();

      return savedMessage;
    },
  },
};
