import mongoose from "mongoose";

const Schema = mongoose.Schema;

const demandeSchema = new Schema(
  {
    from: { type: Schema.Types.ObjectId, required: true, ref: "Users" },
    to: { type: Schema.Types.ObjectId, required: true, ref: "Users" },
    etat: { type: String, required: false, default: "en cours" },
  },
  { timestamps: true }
);

export const DemandeModel = mongoose.model("Demande", demandeSchema);
