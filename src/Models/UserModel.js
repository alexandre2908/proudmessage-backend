import mongoose from "mongoose";

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true },
    telephone: { type: String, required: true },
    password: { type: String, required: true },
    is_online: { type: Boolean, required: false, default: false },
    demandes: [{ type: Schema.Types.ObjectId, ref: "Demande" }],
    contacts: [{ type: Schema.Types.ObjectId, ref: "Users" }],
  },
  { timestamps: true }
);

export const UserModel = mongoose.model("Users", userSchema);
