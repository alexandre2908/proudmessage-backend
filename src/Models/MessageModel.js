import mongoose from "mongoose";

const Schema = mongoose.Schema;

const messageSchema = new Schema(
  {
    from: { type: Schema.Types.ObjectId, required: true, ref: "Users" },
    to: { type: Schema.Types.ObjectId, required: true, ref: "Users" },
    message: { type: String, required: true },
  },
  { timestamps: true }
);

export const MessageModel = mongoose.model("Message", messageSchema);
