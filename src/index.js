import { ApolloServer, gql } from "apollo-server-express";
import express, { request } from "express";
import mongoose from "mongoose";
import { resolvers } from "./Utils/resolvers";
import { typeDefs } from "./Utils/typeDefs";
import isAuth from "./middleware/is-auth";
import bodyParser from "body-parser";

const startServer = async () => {
  const app = express();

  app.use(bodyParser.json());
  app.use(isAuth);
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({ req, res }) => ({ req, res }),
  });

  server.applyMiddleware({ app });

  await mongoose
    .connect(`mongodb://localhost:27017/${process.env.MONGO_DB}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .catch((err) => console.log(err));

  app.listen({ port: 4000 }, () => {
    console.log(`server ready at http://localhost:4000${server.graphqlPath}`);
  });
};

startServer();
